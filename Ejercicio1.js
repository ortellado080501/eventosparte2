//1- Escribir un archivo con extensión .js donde se declare un vector de 6 elementos, usando 3 tipos de datos JS
let vector = [1, 2, "a", "b", true, false];
//a. Imprimir en la consola el vector
console.log(vector);
//b. Imprimir en la consola el primer y el último elemento del vector usando sus índices
console.log("El primer elemento es:", vector[0]);
console.log("El último elemento es:", vector[vector.length - 1]);
//c. Modificar el valor del tercer elemento
vector[2] = "c";
//d. Imprimir en la consola la longitud del vector
console.log("La longitud del vector es:", vector.length);
//e. Agregar un elemento al vector usando "push"
vector.push("5");
//f. Eliminar elemento del final e imprimirlo usando "pop"
let ultimoElemento = vector.pop();
console.log("El último elemento eliminado es:", ultimoElemento);
//g. Agregar un elemento en la mitad del vector usando "splice"
vector.splice(Math.floor(vector.length / 2), 0, "t");
//h. Eliminar el primer elemento usando "shift"
let primerElemento = vector.shift();
console.log("El primer elemento eliminado:", primerElemento);
//i. Agregar de nuevo el mismo elemento al inicio del vector usando "unshift"
vector.unshift(primerElemento);
//Imprimir el vector actualizado en la consola
console.log("El vector actualizado:", vector);
