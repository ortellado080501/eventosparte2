//2- Escribir un archivo con extensión .js donde se declare un vector de 6 elementos, usando 3 tipos de datos JS:
let vector = [1, 2, "a", "b", true, false];
//a. Imprimir en la consola cada valor usando "for"
console.log("for:");
for (let i = 0; i < vector.length; i++) {
  console.log(vector[i]);
}
//b. Idem al anterior usando "forEach"
console.log("forEach:");
vector.forEach(function(elemento) {
  console.log(elemento);
});
//c. Idem al anterior usando "map"
console.log("map:");
vector.map(function(elemento) {
  console.log(elemento);
});
// d. Idem al anterior usando "while"
console.log("while:");
let index = 0;
while (index < vector.length) {
  console.log(vector[index]);
  index++;
}
// e. Idem al anterior usando "for..of"
console.log("for..of:");
for (let elemento of vector) {
  console.log(elemento);
}
